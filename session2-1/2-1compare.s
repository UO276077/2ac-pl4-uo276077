;
; Compares if the length of two strings is the same
;
    .data

string1:       .asciiz "whatever1"   ; first string
string2:       .asciiz "whatever2"   ; second string

    .code

	ld r12, string1(r0) ; save the address of string1 in r12
	jal procedure
	ld r10, r13         ; load the length of string1

   
    ld r12, string2(r0)
	jal procedure
	ld r11, r13         ; load the length of string2


	sltu r8, r10, r11   ; Save if equals/not equals in r8
    halt
	
	
procedure:
	xor r13, r13, r13   ; character count
	
loop:
	lbu r6, r12 		; load next character
	beqz r6, endloop	; end of string?
	daddui r13, r13, 1  ; count++
	daddui r12, r12, 1  ; next index
	j loop	
	
endloop:
	sd r13, result(r0)  ; save result
	jr r31 				; return
