;
; Measures the length of a character string
;
	.data
string: .asciiz "whatever"
result: .word 0

	.text
main:
	xor r13, r13, r13 ; character count
	xor r12, r12, r12 ; index to current character
	
loop:
	lbu r6, string(r12) ; load next character
	beqz r6, end		; end of string?
	daddui r13, r13, 1  ; count++
	daddui r12, r12, 1  ; next index
	j loop	
	
end:
	sd r13, result(r0)  ; save result
	halt
